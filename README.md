# Hermine-data

This repo is meant to host shared data (licences and generic obligations) related to the Hermine project.
It is still very much a work in progress.

Default licence for this repo is  ODbL-1.0.

Download the latest [shared.json](https://gitlab.com/hermine-project/community-data/-/releases/permalink/latest/downloads/shared.json) file and refer to [Hermine's documentation](https://docs.hermine-foss.org/reference_data.html).
